from datetime import timedelta

import pandas as pd

from performanceanalysispy.performance_matrices import PerformanceMatrices


def go_long(rank=30):
    df_shift_weekly_return = pd.read_csv('../data/backtest_data/df_shift_weekly_return.csv', index_col='Date')
    df_shift_weekly_return.index = pd.to_datetime(df_shift_weekly_return.index, format='%Y-%m-%d')

    rank_largest = pd.read_csv(f'../data/backtest_data/{rank}_ranking_largest_weekly.csv', index_col='Dates')
    rank_largest.index = pd.to_datetime(rank_largest.index, format='%Y-%m-%d')

    rank_smallest = pd.read_csv(f'../data/backtest_data/{rank}_ranking_smallest_weekly.csv', index_col='Dates')
    rank_smallest.index = pd.to_datetime(rank_smallest.index, format='%Y-%m-%d')

    col = [str(i + 1) for i in range(rank)]

    df = pd.read_csv('../data/price_data/^HSI-2020_01_01.csv', index_col='Date')
    df.index = pd.to_datetime(df.index, format='%Y-%m-%d')
    adjusted_df = df[['Open', 'Close']]
    adjusted_df_weekly = adjusted_df.resample('W').first()
    ret = adjusted_df_weekly.shift(-1) / adjusted_df_weekly - 1
    ret.index = pd.to_datetime(ret.index, format='%Y-%m-%d')
    ret.dropna(axis=0, how='all', inplace=True)

    long_dicts_list = []
    for i in range(len(rank_largest)):
        date = rank_largest.index[i]

        temp = []
        for c in col:
            temp.append({f'L{c}': rank_largest.iloc[i][c].split('_')[0]})

        long_dicts_list.append(dynamic_long_dict(
            df_shift_weekly_return=df_shift_weekly_return, date=date, d_list=temp))

    l = pd.DataFrame(long_dicts_list)
    l['pnl'] = l.sum(axis=1)/rank
    pnl_long = l['pnl']
    pnl_long.index = pd.to_datetime(l['Date'], format='%Y-%m-%d')
    pnl_long = pnl_long - 0.004
    pnl_long.dropna(inplace=True)
    pm = PerformanceMatrices(pnl_long, resolution='weekly')
    pm.performance_plot()
    pnl_long.to_csv('./../results/long_only_performance_30_easy.csv')


def go_short(rank=30):
    df_shift_weekly_return = pd.read_csv('../data/backtest_data/df_shift_weekly_return.csv', index_col='Date')
    df_shift_weekly_return.index = pd.to_datetime(df_shift_weekly_return.index, format='%Y-%m-%d')

    rank_largest = pd.read_csv(f'../data/backtest_data/{rank}_ranking_largest_weekly.csv', index_col='Dates')
    rank_largest.index = pd.to_datetime(rank_largest.index, format='%Y-%m-%d')

    rank_smallest = pd.read_csv(f'../data/backtest_data/{rank}_ranking_smallest_weekly.csv', index_col='Dates')
    rank_smallest.index = pd.to_datetime(rank_smallest.index, format='%Y-%m-%d')

    col = [str(i + 1) for i in range(rank)]

    df = pd.read_csv('../data/price_data/^HSI-2020_01_01.csv', index_col='Date')
    df.index = pd.to_datetime(df.index, format='%Y-%m-%d')
    adjusted_df = df[['Open', 'Close']]
    adjusted_df_weekly = adjusted_df.resample('W').first()
    ret = adjusted_df_weekly.shift(-1) / adjusted_df_weekly - 1
    ret.index = pd.to_datetime(ret.index, format='%Y-%m-%d')
    ret.dropna(axis=0, how='all', inplace=True)

    short_dicts_list = []
    for i in range(len(rank_smallest)):
        date = rank_smallest.index[i]

        temp = []
        for c in col:
            temp.append({f'L{c}': rank_smallest.iloc[i][c].split('_')[0]})

        short_dicts_list.append(dynamic_long_dict(
            df_shift_weekly_return=df_shift_weekly_return, date=date, d_list=temp))

    s = pd.DataFrame(short_dicts_list)
    s['pnl'] = s.sum(axis=1)/rank
    pnl_short = s['pnl']
    pnl_short.index = pd.to_datetime(s['Date'], format='%Y-%m-%d')
    pnl_short = pnl_short - 0.008 - ret['Open']
    pnl_short.dropna(inplace=True)
    pm = PerformanceMatrices(pnl_short, resolution='weekly')
    pm.performance_plot()
    pnl_short.to_csv('./../results/short_only_performance_30_not.csv')


def filter_keyerror(df_shift_weekly_return, date, number):
    try:
        return df_shift_weekly_return.loc[date + timedelta(days=7), f'{number}_open']
    except Exception as e:
        print(e.__str__())
        return 0.0


def dynamic_long_dict(df_shift_weekly_return, date, d_list):
    results = {'Date': date}
    for d in d_list:
        results[list(d.keys())[0]] = filter_keyerror(
            df_shift_weekly_return=df_shift_weekly_return, date=date, number=list(d.values())[0])

    return results


def dynamic_short_dict(df_shift_weekly_return, date, d_list):
    results = {'Date': date}
    for d in d_list:
        results[list(d.keys())[0]] = filter_keyerror(
            df_shift_weekly_return=df_shift_weekly_return, date=date, number=-list(d.values())[0])

    return results


if __name__ == '__main__':
    go_short(rank=30)

