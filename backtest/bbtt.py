from datetime import timedelta

import pandas as pd
import numpy as np

from performanceanalysispy.performance_matrices import PerformanceMatrices


rank = 30
df = pd.read_csv('./../data/merged_price_data_daily.csv', index_col='Date')
df.dropna(axis=0, how='all', inplace=True)
df.index = pd.to_datetime(df.index, format='%Y-%m-%d')

print(df.head())

df_shift_daily_return = df.shift(-1) / df - 1
df_shift_daily_return.dropna(axis=0, how='all', inplace=True)

print(df_shift_daily_return)
df_shift_daily_return.to_csv('./../data/backtest_data/df_shift_daily_return.csv')

daily_ff_percentage = pd.read_csv('./../data/modified_combined_fund_flow_percentage_daily.csv', index_col='Dates')
daily_ff_percentage.index = pd.to_datetime(daily_ff_percentage.index, dayfirst=True)
n_days = [i for i in range(1, 22)]

df = pd.read_csv('../data/price_data/^HSI-2020_01_01.csv', index_col='Date')
df.index = pd.to_datetime(df.index, format='%Y-%m-%d')

adjusted_df = df[['Open', 'Close']]
ret = adjusted_df.shift(-1) / adjusted_df - 1
ret.index = pd.to_datetime(ret.index, format='%Y-%m-%d')
ret.dropna(axis=0, how='all', inplace=True)


def dynamic_long_dict(df_shift_daily_return, date, d_list):
    results = {'Date': date}
    for d in d_list:
        results[list(d.keys())[0]] = filter_keyerror(
            df_shift_daily_return=df_shift_daily_return, date=date, number=list(d.values())[0])

    return results


def dynamic_short_dict(df_shift_daily_return, date, d_list):
    results = {'Date': date}
    for d in d_list:
        results[list(d.keys())[0]] = filter_keyerror(
            df_shift_daily_return=df_shift_daily_return, date=date, number=-list(d.values())[0])


    return results


def filter_keyerror(df_shift_daily_return, date, number):
    try:
        return df_shift_daily_return.loc[date + timedelta(days=1), f'{number}_open']
    except Exception as e:
        print(e.__str__())
        return 0.0


col = [str(i + 1) for i in range(rank)]

performance = []
for n_day in n_days:
    # n_day = 20

    n_days_ff_percent_difference = daily_ff_percentage.diff(periods=n_day)
    n_days_ff_percent_difference.dropna(axis=0, how='all', inplace=True)

    rank_largest = pd.DataFrame(
        n_days_ff_percent_difference.columns.values[np.argsort(-n_days_ff_percent_difference.values, axis=1)[:, :rank]],
        index=n_days_ff_percent_difference.index,
        columns=col).reset_index()
    rank_largest.index = rank_largest['Dates']
    rank_largest.index = pd.to_datetime(rank_largest.index, dayfirst=True)

    long_dicts_list = []
    for i in range(len(rank_largest)):
        date = rank_largest.index[i]

        temp = []
        for c in col:
            temp.append({f'L{c}': rank_largest.iloc[i][c].split('_')[0]})

        long_dicts_list.append(dynamic_long_dict(
            df_shift_daily_return=df_shift_daily_return, date=date, d_list=temp))

    l = pd.DataFrame(long_dicts_list)
    l['pnl'] = l.sum(axis=1) / rank
    pnl_long = l['pnl']
    pnl_long.index = pd.to_datetime(l['Date'], format='%Y-%m-%d')
    pnl_long = pnl_long - 0.004
    pnl_long.dropna(inplace=True)
    pm = PerformanceMatrices(pnl_long, resolution='daily')
    # pm.performance_plot()
    # pnl_long.to_csv('./long_only_performance_120.csv')

    performance.append({'day_param': n_day, 'sharpe': pm.fetch_annualized_sharpe_ratio()})

p = pd.DataFrame(performance)
print(p)
