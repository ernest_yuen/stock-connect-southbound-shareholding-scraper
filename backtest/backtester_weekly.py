from datetime import timedelta

import pandas as pd

from performanceanalysispy.performance_matrices import PerformanceMatrices

from backtest.util import long_dict, short_dict


def generate_shift_weekly_open_return():
    df = pd.read_csv('./../data/merged_price_data_daily.csv', index_col='Date')
    df.dropna(axis=0, how='all', inplace=True)
    df.index = pd.to_datetime(df.index, format='%Y-%m-%d')

    print(df)

    df_weekly = df.resample('W').first()
    print(df_weekly)

    # df_weekly_return = df_weekly.pct_change()
    # df_weekly_return.dropna(axis=0, how='all', inplace=True)
    # print(df_weekly_return)

    df_shift_weekly_return = df_weekly.shift(-1)/df_weekly - 1
    df_shift_weekly_return.dropna(axis=0, how='all', inplace=True)
    print(df_shift_weekly_return)
    df_shift_weekly_return.to_csv('./../daily_fund_flow_snapshot/df_shift_weekly_return.csv')


def backtest_fund_flow_long_short():
    df_shift_weekly_return = pd.read_csv('../data/backtest_data/df_shift_weekly_return.csv', index_col='Date')
    df_shift_weekly_return.index = pd.to_datetime(df_shift_weekly_return.index, format='%Y-%m-%d')

    rank_largest = pd.read_csv('../data/backtest_data/ranking_largest_weekly.csv', index_col='Dates')
    rank_largest.index = pd.to_datetime(rank_largest.index, format='%Y-%m-%d')

    rank_smallest = pd.read_csv('../data/backtest_data/ranking_smallest_weekly.csv', index_col='Dates')
    rank_smallest.index = pd.to_datetime(rank_smallest.index, format='%Y-%m-%d')

    df = pd.read_csv('../data/price_data/^HSI-2020_01_01.csv', index_col='Date')
    df.index = pd.to_datetime(df.index, format='%Y-%m-%d')
    adjusted_df = df[['Open', 'Close']]
    adjusted_df_weekly = adjusted_df.resample('W').first()
    ret = adjusted_df_weekly.shift(-1) / adjusted_df_weekly - 1
    ret.index = pd.to_datetime(ret.index, format='%Y-%m-%d')
    ret.dropna(axis=0, how='all', inplace=True)

    long_dicts_list = []
    for i in range(len(rank_largest)):
        date = rank_largest.index[i]
        first_no = rank_largest.iloc[i]['1st Max'].split('_')[0]
        second_no = rank_largest.iloc[i]['2nd Max'].split('_')[0]
        third_no = rank_largest.iloc[i]['3rd Max'].split('_')[0]
        forth_no = rank_largest.iloc[i]['4th Max'].split('_')[0]
        fifth_no = rank_largest.iloc[i]['5th Max'].split('_')[0]

        long_dicts_list.append(long_dict(
            date=date,
            l_1=filter_keyerror(df_shift_weekly_return=df_shift_weekly_return, date=date, number=first_no),
            l_2=filter_keyerror(df_shift_weekly_return=df_shift_weekly_return, date=date, number=second_no),
            l_3=filter_keyerror(df_shift_weekly_return=df_shift_weekly_return, date=date, number=third_no),
            l_4=filter_keyerror(df_shift_weekly_return=df_shift_weekly_return, date=date, number=forth_no),
            l_5=filter_keyerror(df_shift_weekly_return=df_shift_weekly_return, date=date, number=fifth_no)))

    l = pd.DataFrame(long_dicts_list)
    l['pnl'] = l.sum(axis=1)/5
    pnl_long = l['pnl']
    pnl_long.index = pd.to_datetime(l['Date'], format='%Y-%m-%d')
    pnl_long = pnl_long - ret['Open'] - 0.004
    pnl_long.dropna(inplace=True)
    pm = PerformanceMatrices(pnl_long, resolution='weekly')
    pm.performance_plot()
    pnl_long.to_csv('./../daily_fund_flow_snapshot/long_only_performance_20.csv')

    # short_dicts_list = []
    # for i in range(len(rank_smallest)):
    #     date = rank_smallest.index[i]
    #     first_no = rank_smallest.iloc[i]['1st Min'].split('_')[0]
    #     second_no = rank_smallest.iloc[i]['2nd Min'].split('_')[0]
    #     third_no = rank_smallest.iloc[i]['3rd Min'].split('_')[0]
    #     forth_no = rank_smallest.iloc[i]['4th Min'].split('_')[0]
    #     fifth_no = rank_smallest.iloc[i]['5th Min'].split('_')[0]
    #
    #     short_dicts_list.append(short_dict(
    #         date=date,
    #         s_1=filter_keyerror(df_shift_weekly_return=df_shift_weekly_return, date=date, number=first_no),
    #         s_2=filter_keyerror(df_shift_weekly_return=df_shift_weekly_return, date=date, number=second_no),
    #         s_3=filter_keyerror(df_shift_weekly_return=df_shift_weekly_return, date=date, number=third_no),
    #         s_4=filter_keyerror(df_shift_weekly_return=df_shift_weekly_return, date=date, number=forth_no),
    #         s_5=filter_keyerror(df_shift_weekly_return=df_shift_weekly_return, date=date, number=fifth_no)))
    #
    # s = pd.DataFrame(short_dicts_list)
    # s['pnl'] = s.sum(axis=1)/5
    # pnl_short = s['pnl']
    # pnl_short.index = pd.to_datetime(s['Date'], format='%Y-%m-%d')
    # pm = PerformanceMatrices(pnl_short, resolution='weekly')
    # pm.performance_plot()
    # pnl_short.to_csv('./../daily_fund_flow_snapshot/short_only_performance_2.csv')


def filter_keyerror(df_shift_weekly_return, date, number):
    try:
        return df_shift_weekly_return.loc[date + timedelta(days=7), f'{number}_open']
    except Exception as e:
        print(e.__str__())
        return 0.0


if __name__ == '__main__':
    # generate_shift_weekly_open_return()
    backtest_fund_flow_long_short()
