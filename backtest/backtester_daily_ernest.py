from datetime import timedelta

import pandas as pd

from performanceanalysispy.performance_matrices import PerformanceMatrices

from backtest.util import long_dict, short_dict


def generate_df_shift_daily_return():
    df = pd.read_csv('./../data/merged_price_data_daily.csv', index_col='Date')
    df.dropna(axis=0, how='all', inplace=True)
    df.index = pd.to_datetime(df.index, format='%Y-%m-%d')

    print(df.head())

    df_shift_return = df.shift(-1)/df - 1
    df_shift_return.dropna(axis=0, how='all', inplace=True)

    print(df_shift_return)
    df_shift_return.to_csv('./../data/backtest_data/df_shift_daily_return.csv')


def backtest_ff_daily():
    df_shift_daily_return = pd.read_csv('./../data/backtest_data/df_shift_daily_return.csv', index_col='Date')
    df_shift_daily_return.index = pd.to_datetime(df_shift_daily_return.index, format='%Y-%m-%d')

    rank_largest = pd.read_csv('../data/backtest_data/ranking_largest_daily.csv', index_col='Dates')
    rank_largest.index = pd.to_datetime(rank_largest.index, dayfirst=True)

    rank_smallest = pd.read_csv('../data/backtest_data/ranking_smallest_daily.csv', index_col='Dates')
    rank_smallest.index = pd.to_datetime(rank_smallest.index, dayfirst=True)

    df = pd.read_csv('../data/price_data/^HSI-2020_01_01.csv', index_col='Date')
    df.index = pd.to_datetime(df.index, format='%Y-%m-%d')

    adjusted_df = df[['Open', 'Close']]
    ret = adjusted_df.shift(-1) / adjusted_df - 1
    ret.index = pd.to_datetime(ret.index, format='%Y-%m-%d')
    ret.dropna(axis=0, how='all', inplace=True)

    long_dicts_list = []
    for i in range(len(rank_largest)):
        date = rank_largest.index[i]
        first_no = rank_largest.iloc[i]['1st Max'].split('_')[0]
        second_no = rank_largest.iloc[i]['2nd Max'].split('_')[0]
        third_no = rank_largest.iloc[i]['3rd Max'].split('_')[0]
        forth_no = rank_largest.iloc[i]['4th Max'].split('_')[0]
        fifth_no = rank_largest.iloc[i]['5th Max'].split('_')[0]

        long_dicts_list.append(long_dict(
            date=date,
            l_1=filter_keyerror(df_shift_daily_return=df_shift_daily_return, date=date, number=first_no),
            l_2=filter_keyerror(df_shift_daily_return=df_shift_daily_return, date=date, number=second_no),
            l_3=filter_keyerror(df_shift_daily_return=df_shift_daily_return, date=date, number=third_no),
            l_4=filter_keyerror(df_shift_daily_return=df_shift_daily_return, date=date, number=forth_no),
            l_5=filter_keyerror(df_shift_daily_return=df_shift_daily_return, date=date, number=fifth_no)))

    l = pd.DataFrame(long_dicts_list)
    l['pnl'] = l.sum(axis=1)/5
    pnl_long = l['pnl']
    pnl_long.index = pd.to_datetime(l['Date'], format='%Y-%m-%d')
    pnl_long = - pnl_long + ret['Open'] - 0.008
    pnl_long.dropna(inplace=True)
    pm = PerformanceMatrices(pnl_long, resolution='daily')
    pm.performance_plot()
    pnl_long.to_csv('./long_only_performance_0.csv')


def filter_keyerror(df_shift_daily_return, date, number):
    try:
        return df_shift_daily_return.loc[date + timedelta(days=1), f'{number}_open']
    except Exception as e:
        print(e.__str__())
        return 0.0


if __name__ == '__main__':
    backtest_ff_daily()
