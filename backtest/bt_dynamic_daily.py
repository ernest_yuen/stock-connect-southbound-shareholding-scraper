from datetime import timedelta

import pandas as pd

from performanceanalysispy.performance_matrices import PerformanceMatrices


def go_long(rank=30):
    n_days = [i for i in range(1, 22)]
    performance = []

    df_shift_daily_return = pd.read_csv('./../data/backtest_data/df_shift_daily_return.csv', index_col='Date')
    df_shift_daily_return.index = pd.to_datetime(df_shift_daily_return.index, format='%Y-%m-%d')

    rank_largest = pd.read_csv('../data/backtest_data/30_ranking_largest_daily.csv', index_col='Dates')
    rank_largest.index = pd.to_datetime(rank_largest.index, dayfirst=True)

    rank_smallest = pd.read_csv('../data/backtest_data/30_ranking_smallest_daily.csv', index_col='Dates')
    rank_smallest.index = pd.to_datetime(rank_smallest.index, dayfirst=True)

    col = [str(i + 1) for i in range(rank)]

    df = pd.read_csv('../data/price_data/^HSI-2020_01_01.csv', index_col='Date')
    df.index = pd.to_datetime(df.index, format='%Y-%m-%d')

    adjusted_df = df[['Open', 'Close']]
    ret = adjusted_df.shift(-1) / adjusted_df - 1
    ret.index = pd.to_datetime(ret.index, format='%Y-%m-%d')
    ret.dropna(axis=0, how='all', inplace=True)

    long_dicts_list = []
    for i in range(len(rank_largest)):
        date = rank_largest.index[i]

        temp = []
        for c in col:
            temp.append({f'L{c}': rank_largest.iloc[i][c].split('_')[0]})

        long_dicts_list.append(dynamic_long_dict(
            df_shift_daily_return=df_shift_daily_return, date=date, d_list=temp))

    l = pd.DataFrame(long_dicts_list)
    l['pnl'] = l.sum(axis=1)/rank
    pnl_long = l['pnl']
    pnl_long.index = pd.to_datetime(l['Date'], format='%Y-%m-%d')
    pnl_long = pnl_long - 0.004
    pnl_long.dropna(inplace=True)
    pm = PerformanceMatrices(pnl_long, resolution='daily')
    # pm.performance_plot()
    # pnl_long.to_csv('./../results/long_only_performance_30_easy_daily.csv')

    performance.append({'day_param': n_day, 'sharpe': pm.fetch_annualized_sharpe_ratio()})



def filter_keyerror(df_shift_daily_return, date, number):
    try:
        return df_shift_daily_return.loc[date + timedelta(days=1), f'{number}_open']
    except Exception as e:
        print(e.__str__())
        return 0.0


def dynamic_long_dict(df_shift_daily_return, date, d_list):
    results = {'Date': date}
    for d in d_list:
        results[list(d.keys())[0]] = filter_keyerror(
            df_shift_daily_return=df_shift_daily_return, date=date, number=list(d.values())[0])

    return results


def dynamic_short_dict(df_shift_daily_return, date, d_list):
    results = {'Date': date}
    for d in d_list:
        results[list(d.keys())[0]] = filter_keyerror(
            df_shift_daily_return=df_shift_daily_return, date=date, number=-list(d.values())[0])

    return results


if __name__ == '__main__':
    go_long()
