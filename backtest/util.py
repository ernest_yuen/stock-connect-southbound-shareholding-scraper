def long_dict(date, l_1, l_2, l_3, l_4, l_5):
    return {'Date': date, 'L1': l_1, 'L2': l_2, 'L3': l_3, 'L4': l_4, 'L5': l_5}


def short_dict(date, s_1, s_2, s_3, s_4, s_5):
    return {'Date': date, 'S1': -s_1, 'S2': -s_2, 'S3': -s_3, 'S4': -s_4, 'S5': -s_5}
