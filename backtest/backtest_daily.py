from datetime import timedelta

import pandas as pd
import numpy as np

from performanceanalysispy.performance_matrices import PerformanceMatrices

from backtest.util import long_dict, short_dict

df = pd.read_csv('./../data/merged_price_data_daily.csv', index_col='Date')
df.dropna(axis=0, how='all', inplace=True)
df.index = pd.to_datetime(df.index, format='%Y-%m-%d')

print(df.head())

df_shift_daily_return = df.shift(-1) / df - 1
df_shift_daily_return.dropna(axis=0, how='all', inplace=True)

print(df_shift_daily_return)
df_shift_daily_return.to_csv('./../data/backtest_data/df_shift_daily_return.csv')

daily_ff_percentage = pd.read_csv('./../data/modified_combined_fund_flow_percentage_daily.csv', index_col='Dates')
daily_ff_percentage.index = pd.to_datetime(daily_ff_percentage.index, dayfirst=True)
n_days = [i for i in range(1, 22)]

df = pd.read_csv('../data/price_data/^HSI-2020_01_01.csv', index_col='Date')
df.index = pd.to_datetime(df.index, format='%Y-%m-%d')

adjusted_df = df[['Open', 'Close']]
ret = adjusted_df.shift(-1) / adjusted_df - 1
ret.index = pd.to_datetime(ret.index, format='%Y-%m-%d')
ret.dropna(axis=0, how='all', inplace=True)


def filter_keyerror(df_shift_daily_return, date, number):
    try:
        return df_shift_daily_return.loc[date + timedelta(days=1), f'{number}_open']
    except Exception as e:
        print(e.__str__())
        return 0.0

performance = []
for n_day in n_days:
    # n_day = 20

    n_days_ff_percent_difference = daily_ff_percentage.diff(periods=n_day)
    n_days_ff_percent_difference.dropna(axis=0, how='all', inplace=True)

    rank_largest = pd.DataFrame(
        n_days_ff_percent_difference.columns.values[np.argsort(-n_days_ff_percent_difference.values, axis=1)[:, :5]],
        index=n_days_ff_percent_difference.index,
        columns=['1st Max', '2nd Max', '3rd Max', '4th Max', '5th Max']).reset_index()
    rank_largest.index = rank_largest['Dates']
    rank_largest.index = pd.to_datetime(rank_largest.index, dayfirst=True)

    # smallest_df = pd.DataFrame(
    #     n_days_ff_percent_difference.columns.values[np.argsort(n_days_ff_percent_difference.values, axis=1)[:, :5]],
    #     index=n_days_ff_percent_difference.index,
    #     columns=['1st Min', '2nd Min', '3rd Min', '4th Min', '5th Min']).reset_index()

    long_dicts_list = []
    for i in range(len(rank_largest)):
        date = rank_largest.index[i]
        first_no = rank_largest.iloc[i]['1st Max'].split('_')[0]
        second_no = rank_largest.iloc[i]['2nd Max'].split('_')[0]
        third_no = rank_largest.iloc[i]['3rd Max'].split('_')[0]
        forth_no = rank_largest.iloc[i]['4th Max'].split('_')[0]
        fifth_no = rank_largest.iloc[i]['5th Max'].split('_')[0]

        long_dicts_list.append(long_dict(
            date=date,
            l_1=filter_keyerror(df_shift_daily_return=df_shift_daily_return, date=date, number=first_no),
            l_2=filter_keyerror(df_shift_daily_return=df_shift_daily_return, date=date, number=second_no),
            l_3=filter_keyerror(df_shift_daily_return=df_shift_daily_return, date=date, number=third_no),
            l_4=filter_keyerror(df_shift_daily_return=df_shift_daily_return, date=date, number=forth_no),
            l_5=filter_keyerror(df_shift_daily_return=df_shift_daily_return, date=date, number=fifth_no)))

    l = pd.DataFrame(long_dicts_list)
    l['pnl'] = l.sum(axis=1) / 5
    pnl_long = l['pnl']
    pnl_long.index = pd.to_datetime(l['Date'], format='%Y-%m-%d')
    pnl_long = pnl_long - ret['Open'] - 0.008
    pnl_long.dropna(inplace=True)
    pm = PerformanceMatrices(pnl_long, resolution='daily')
    # pm.performance_plot()
    # pnl_long.to_csv('./long_only_performance_120.csv')

    performance.append({'day_param': n_day, 'sharpe': pm.fetch_annualized_sharpe_ratio()})

p = pd.DataFrame(performance)
print(p)
