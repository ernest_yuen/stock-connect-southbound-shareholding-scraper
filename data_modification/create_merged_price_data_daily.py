import os

import pandas as pd


def create_merged_price_data_daily():
    entries = os.listdir('./../data/price_data/')
    target_csvs = []

    for name in entries:
        if name.split('-')[1] == '2020_01_01.csv':
            target_csvs.append(name)

    df = pd.read_csv(f"../data/price_data/{target_csvs[0]}", index_col='Date')
    df.index = pd.to_datetime(df.index, format='%Y-%m-%d')
    # universal_date_index = [datetime.strptime(d, '%Y-%m-%d') for d in df.loc[:, 'Date']]

    combined_price_df = df[['Open', 'Close']]
    combined_price_df.rename(columns={'Open': f'{target_csvs[0].split("-")[0]}_open',
                                      'Close': f'{target_csvs[0].split("-")[0]}_close'}, inplace=True)

    for count, files in enumerate(target_csvs):
        if count == 0:
            continue

        if count == len(target_csvs)-1:
            break

        try:
            single_df = pd.read_csv(f"../data/price_data/{files}", index_col='Date')
            single_df.index = pd.to_datetime(single_df.index, format='%Y-%m-%d')

            useful_single_df = single_df[['Open', 'Close']]
            useful_single_df.rename(columns={'Open': f'{files.split("-")[0]}_open',
                                             'Close': f'{files.split("-")[0]}_close'}, inplace=True)
            combined_price_df = pd.concat([combined_price_df, useful_single_df], axis=1, join='outer')

        except Exception as e:
            print(e.__str__())
            continue

    combined_price_df.to_csv('./../data/merged_price_data_daily.csv')


if __name__ == '__main__':
    create_merged_price_data_daily()
