import pandas as pd


def resample_2_weekly_data():
    df = pd.read_csv('./../data/modified_combined_fund_flow_percentage_daily.csv', index_col='Dates')
    df.index = pd.to_datetime(df.index, dayfirst=True)
    df_weekly = df.resample('W').last()
    print(df_weekly.index)

    diff = df_weekly.diff()
    df_weekly['Dates'] = df_weekly.index
    diff['Dates'] = df_weekly.index

    df_weekly.to_csv('./../modified_combined_fund_flow_percentage_weekly.csv', index=False)
    diff.to_csv('./../backtest_data/diff_combined_weekly.csv', index=False)


if __name__ == '__main__':
    resample_2_weekly_data()
