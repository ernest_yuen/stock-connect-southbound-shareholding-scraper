import pandas as pd
import numpy as np


def create_weekly_ranking():
    df = pd.read_csv('../data/backtest_data/diff_combined_weekly.csv')
    df = df.set_index('Dates')
    df.dropna(axis=0, how='all', inplace=True)

    largest_df = pd.DataFrame(df.columns.values[np.argsort(-df.values, axis=1)[:, :5]],
                              index=df.index,
                              columns=['1st Max', '2nd Max', '3rd Max', '4th Max', '5th Max']).reset_index()
    # largest_df.drop(largest_df.index[0], inplace=True)
    print(largest_df)
    largest_df.to_csv('./../data/backtest_data/ranking_largest_weekly.csv')

    smallest_df = pd.DataFrame(df.columns.values[np.argsort(df.values, axis=1)[:, :5]],
                               index=df.index,
                               columns=['1st Min', '2nd Min', '3rd Min', '4th Min', '5th Min']).reset_index()
    # smallest_df.drop(smallest_df.index[0], inplace=True)
    print(smallest_df)
    smallest_df.to_csv('./../data/backtest_data/ranking_smallest_weekly.csv')


def create_daily_ranking():
    df = pd.read_csv('./../data/backtest_data/diff_combined_daily.csv')
    df = df.set_index('Dates')
    df.dropna(axis=0, how='all', inplace=True)

    largest_df = pd.DataFrame(df.columns.values[np.argsort(-df.values, axis=1)[:, :5]],
                              index=df.index,
                              columns=['1st Max', '2nd Max', '3rd Max', '4th Max', '5th Max']).reset_index()
    # largest_df.drop(largest_df.index[0], inplace=True)
    print(largest_df)
    largest_df.to_csv('./../data/backtest_data/ranking_largest_daily.csv')

    smallest_df = pd.DataFrame(df.columns.values[np.argsort(df.values, axis=1)[:, :5]],
                               index=df.index,
                               columns=['1st Min', '2nd Min', '3rd Min', '4th Min', '5th Min']).reset_index()
    # smallest_df.drop(smallest_df.index[0], inplace=True)
    print(smallest_df)
    smallest_df.to_csv('./../data/backtest_data/ranking_smallest_daily.csv')


if __name__ == '__main__':
    create_daily_ranking()
