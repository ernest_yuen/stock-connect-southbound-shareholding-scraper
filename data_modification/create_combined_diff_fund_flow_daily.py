import pandas as pd


def main(input_data_relative_path='./../data/modified_combined_fund_flow_percentage_daily.csv',
         target_csv_path='./../data/backtest_data/diff_combined_daily.csv'):
    df = pd.read_csv(input_data_relative_path, index_col='Dates')
    diff = df.diff()
    diff.to_csv(target_csv_path)


if __name__ == '__main__':
    main()
