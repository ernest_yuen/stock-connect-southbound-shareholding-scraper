import pandas as pd
import numpy as np


def create_weekly_ranking(rank=30):
    df = pd.read_csv('../data/backtest_data/diff_combined_weekly.csv')
    df = df.set_index('Dates')
    df.dropna(axis=0, how='all', inplace=True)

    col = [str(i + 1) for i in range(rank)]

    largest_df = pd.DataFrame(df.columns.values[np.argsort(-df.values, axis=1)[:, :rank]],
                              index=df.index,
                              columns=col).reset_index()
    print(largest_df)
    largest_df.to_csv(f'./../data/backtest_data/{rank}_ranking_largest_weekly.csv')

    smallest_df = pd.DataFrame(df.columns.values[np.argsort(df.values, axis=1)[:, :rank]],
                               index=df.index,
                               columns=col).reset_index()
    print(smallest_df)
    smallest_df.to_csv(f'./../data/backtest_data/{rank}_ranking_smallest_weekly.csv')


def create_daily_ranking(rank=30):
    df = pd.read_csv('./../data/backtest_data/diff_combined_daily.csv')
    df = df.set_index('Dates')
    df.dropna(axis=0, how='all', inplace=True)

    col = [str(i + 1) for i in range(rank)]

    largest_df = pd.DataFrame(df.columns.values[np.argsort(-df.values, axis=1)[:, :rank]],
                              index=df.index,
                              columns=col).reset_index()
    print(largest_df)
    largest_df.to_csv(f'./../data/backtest_data/{rank}_ranking_largest_daily.csv')

    smallest_df = pd.DataFrame(df.columns.values[np.argsort(df.values, axis=1)[:, :rank]],
                               index=df.index,
                               columns=col).reset_index()
    print(smallest_df)
    smallest_df.to_csv(f'./../data/backtest_data/{rank}_ranking_smallest_daily.csv')


if __name__ == '__main__':
    create_weekly_ranking()
    create_daily_ranking()
