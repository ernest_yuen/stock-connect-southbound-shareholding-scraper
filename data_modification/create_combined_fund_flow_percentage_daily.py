import pandas as pd


def create_combined_fund_flow_percentage_daily(
        combined_ff_daily_csv_relative_path='./../data/combined_fund_flow_daily.csv',
        target_csv_path='./../data/combined_fund_flow_percentage_daily.csv'):
    df = pd.read_csv(combined_ff_daily_csv_relative_path, index_col=0)
    new_cols = []

    for col in df.columns:
        if col == 'Dates':
            new_cols.append(col)
            continue
        if col.split('_')[1] != 'ccass':
            new_cols.append(col)

    useful_df = df[new_cols]
    useful_df.to_csv(target_csv_path, index=False)


if __name__ == '__main__':
    create_combined_fund_flow_percentage_daily()
