import logging
import os

import pandas as pd
from bs4 import BeautifulSoup


def get_logger(name="root", loglevel=logging.DEBUG):
    logger = logging.getLogger(name)
    logger.setLevel(loglevel)
    ch = logging.StreamHandler()
    ch.setLevel(loglevel)
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s.%(funcName)s - %(levelname)s - %(message)s"
    )
    ch.setFormatter(formatter)

    for h in logger.handlers:
        logger.removeHandler(h)

    logger.addHandler(ch)

    return logger


def stock_dict(stock_code, stock_name, ccass_shareholding, percentage_of_total_no_of_issued_shares):
    return {'Stock Code': stock_code,
            'Stock Name': stock_name,
            'Shareholding in CCASS': ccass_shareholding,
            'Percentage of the total number of Issued Shares': percentage_of_total_no_of_issued_shares}


def content_scrape(page_source, folder_directory, today_only=False):
    if today_only:
        import requests

        link = 'https://www.hkexnews.hk/sdw/search/mutualmarket.aspx?t=hk'
        response = requests.get(url=link)
        soup = BeautifulSoup(response.content, 'html.parser')

        stock_details = soup.find_all(class_='mobile-list-body')
        shareholding_date = soup.find_all(class_='ccass-heading')[0].find_all(
            style="text-decoration:underline;")[0].text.split(': ')[1]

        contents = []
        for i in range(int(len(stock_details) / 4)):
            contents.append(
                stock_dict(stock_details[4 * i].text, stock_details[4 * i + 1].text,
                           stock_details[4 * i + 2].text, stock_details[4 * i + 3].text))

        df = pd.DataFrame(contents)

        if not os.path.exists(f'{folder_directory}'):
            os.makedirs(f'{folder_directory}', exist_ok=True)

        df.to_csv(f"{folder_directory}/"
                  f"{shareholding_date.replace('/', '')}_stock_connect_southbound_shareholding.csv",
                  index=False)
    else:
        soup = BeautifulSoup(page_source, 'html.parser')

        stock_details = soup.find_all(class_='mobile-list-body')
        shareholding_date = soup.find_all(class_='ccass-heading')[0].find_all(
            style="text-decoration:underline;")[0].text.split(': ')[1]

        contents = []
        for i in range(int(len(stock_details) / 4)):
            contents.append(
                stock_dict(stock_details[4 * i].text, stock_details[4 * i + 1].text,
                           stock_details[4 * i + 2].text, stock_details[4 * i + 3].text))

        df = pd.DataFrame(contents)

        if not os.path.exists(f'{folder_directory}'):
            os.makedirs(f'{folder_directory}', exist_ok=True)

        df.to_csv(f"{folder_directory}/"
                  f"{shareholding_date.replace('/', '')}_stock_connect_southbound_shareholding.csv",
                  index=False)
