import os
from datetime import datetime

import pandas as pd
# https://realpython.com/working-with-files-in-python/


def format_dict(file_name: str, date: str):
    return {'filename': file_name, 'shareholding_date': date}


def form_column_names(df, stock_code: int):
    return {f'{stock_code}_ccass_shareholding':
                df.loc[df['Stock Code'] == stock_code, 'Shareholding in CCASS'].values[0],
            f'{stock_code}_percentage':
                df.loc[df['Stock Code'] == stock_code, 'Percentage of the total number of Issued Shares'].values[0]}


def main(relative_daily_fund_flow_snapshot_folder_path='../data/daily_fund_flow_snapshot/',
         target_saved_csv_path='./../data/combined_fund_flow_daily.csv'):
    entries = os.listdir(relative_daily_fund_flow_snapshot_folder_path)

    data = []
    for filename in entries:
        data.append(format_dict(file_name=filename, date=filename[0:8]))

    sorted_data = sorted(data, key=lambda x: datetime.strptime(x['shareholding_date'], '%Y%m%d'))
    date_index = [datetime.strptime(d['shareholding_date'], '%Y%m%d') for d in sorted_data]

    combined_data = []
    for files in sorted_data:
        single_df = pd.read_csv(f'{relative_daily_fund_flow_snapshot_folder_path}{files["filename"]}')

        d = {}
        for sc in single_df['Stock Code']:
            d.update(form_column_names(df=single_df, stock_code=sc))

        combined_data.append(d)

    combined_df = pd.DataFrame(combined_data, index=date_index)
    combined_df['Dates'] = date_index
    print(combined_df)
    combined_df.to_csv(target_saved_csv_path)


if __name__ == '__main__':
    main()
