import os
from selenium import webdriver

from data_handling.util import content_scrape

PARENTS_PATH = os.path.dirname(os.path.dirname(__file__))
driver = webdriver.Chrome(executable_path=f'{PARENTS_PATH}/tools/chromedriver.exe')
link = 'https://www.hkexnews.hk/sdw/search/mutualmarket.aspx?t=hk'
driver.get(link)


def choose_date_and_submit(y, m, d):
    date_button = driver.find_element_by_id('txtShareholdingDate')
    date_button.click()

    year_button = driver.find_element_by_xpath(f'//*[@id="date-picker"]/div[1]/b[1]/ul/li[{y}]/button')
    year_button.click()

    month_button = driver.find_element_by_xpath(f'//*[@id="date-picker"]/div[1]/b[2]/ul/li[{m}]/button')
    month_enable = driver.find_element_by_xpath(
        f'//*[@id="date-picker"]/div[1]/b[2]/ul/li[{m}]/button').is_enabled()
    if not month_enable:
        return False

    elif month_enable:
        month_button.click()

        day_button = driver.find_element_by_xpath(f'//*[@id="date-picker"]/div[1]/b[3]/ul/li[{d}]/button')
        day_enable = driver.find_element_by_xpath(
            f'//*[@id="date-picker"]/div[1]/b[3]/ul/li[{d}]/button').is_enabled()
        if not day_enable:
            return False
        elif day_enable:
            day_button.click()

            submit_button = driver.find_element_by_id('btnSearch')
            submit_button.click()

            return True


def main(today_only=True, folder_directory='./daily_fund_flow_snapshot'):
    if today_only:
        content_scrape(page_source=None, folder_directory=folder_directory, today_only=today_only)
    else:
        years = [1, 2]
        months_31 = [1, 3, 5, 7, 8, 10, 12]
        months_30 = [4, 6, 9, 11]
        months_29 = 2

        days_30 = list(range(1, 31))
        days_31 = list(range(1, 32))
        days_29 = list(range(1, 30))

        for year in years:
            for month in months_30:
                for day in days_30:
                    result = choose_date_and_submit(y=year, m=month, d=day)
                    if result:
                        print(f'{year}-{month}-{day}')
                        content_scrape(driver.page_source, folder_directory=folder_directory, today_only=today_only)

            for month in months_31:
                for day in days_31:
                    result = choose_date_and_submit(y=year, m=month, d=day)
                    if result:
                        print(f'{year}-{month}-{day}')
                        content_scrape(driver.page_source, folder_directory=folder_directory, today_only=today_only)

            for day in days_29:
                result = choose_date_and_submit(y=year, m=months_29, d=day)
                if result:
                    print(f'{year}-{months_29}-{day}')
                    content_scrape(driver.page_source, folder_directory=folder_directory, today_only=today_only)

        driver.quit()


if __name__ == '__main__':
    main()
