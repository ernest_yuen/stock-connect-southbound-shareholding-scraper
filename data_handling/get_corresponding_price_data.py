import time
import yfinance as yf


def main():
    f = open("./../stock_codes.txt", "r")
    content = f.read()
    contents = eval(content)
    stock_codes = [format(int(sc), '04d') for sc in contents]
    hk_stock_yf_codes = [f'{sc}.HK' for sc in stock_codes]
    hk_stock_yf_codes.append('^HSI')
    print(hk_stock_yf_codes)

    years = [2015, 2020]

    for year in years:
        for sc in hk_stock_yf_codes:
            try:
                stock = yf.Ticker(sc)
                if sc == '^HSI':
                    stock.history(start=f'{year}-01-01').to_csv(f'./price_data/{sc}-{year}_01_01.csv')
                    print(f'{sc}: is saved')
                    time.sleep(1)
                    continue
                stock.history(start=f'{year}-01-01').to_csv(f'./price_data/{int(sc[:4])}-{year}_01_01.csv')
                print(f'{sc}: is saved')
                time.sleep(1)

            except Exception as e:
                print(f'{sc}: {e}')
                time.sleep(1)


if __name__ == '__main__':
    main()
